# -*- perl -*-

# uses:
#     $specpath    whole path from caller, minus any leading /s
#     $spechost    host from caller
#
# sets:
#
#  always:
#     $uri
#
#  if match found for this host and path:
#     $serve_user   username, or undef if no match (then other serve_* invalid)
#     $serve_dir    directory as specified in config
#     $serve_repo   subpath under $serve_dir _including_ leading /
#
#  for use by user's service program
#     $repo_regexp
#     $require_exportok

sub remain_path ($) {
    # return value matches {( / [^/]+ )+}x
    my ($vsubpath) = @_;
    syslog 'debug', sprintf "DEBUG remain_path %s $specpath",
                              (defined $vsubpath ? $vsubpath : '<undef>');
    return "/$specpath" if !defined $vsubpath;
    return "" if $vsubpath eq $specpath;
    return substr($specpath,length($vsubpath))
	if substr($specpath,0,length($vsubpath)+1) eq "$vsubpath/";
    return undef;
}

fail "no config ??" unless @ARGV;
fail "no specpath ??" unless length $specpath;

our $uri = "git://$spechost/$specpath";

our $repo_regexp= '^(\\w[-+._0-9A-Za-z]*/?\.git)$';  # stupid emacs ';
our $check_export= 0;

our ($serve_user, $serve_dir, $serve_repo);

sub fexists ($) {
    my ($f) = @_;
    if (stat $f) {
	-f _ or fail "bad config $_ - not a file";
	return 1;
    } else {
	$!==&ENOENT or fail "bad config $_ - could not stat: $!";
	return 0;
    }
}

@ARGV = grep { fexists($_) } @ARGV;

while (<>) {

    s/^\s*//;
    s/\s+$//;
    next unless m/\S/;
    next if m/^\#/;

    if (m{^ single-user \s+ (\S+?) (/\S*)? \s+ (\S+) (?: \s+ (\S+) )? $ }x) {
	my ($h,$v,$u,$d) = ($1,$2,$3,$4);
	next unless $h eq $spechost;
	$serve_repo= remain_path($v);
	next unless defined $serve_repo;
	$serve_user= $u;
	$serve_dir= $d;
        syslog 'debug', "DEBUG $ARGV:$. match".
            " $serve_user $serve_dir $serve_repo";
    } elsif (m{^ multi-user \s+ (\S+?) (/\S*)? \s+ (\S+) $ }x) {
	my ($h,$v,$d) = ($1,$2,$3);
	next unless $1 eq $spechost;
	$serve_repo= remain_path($v);
	next unless defined $serve_repo;
        syslog 'debug', "DEBUG $ARGV:$. perhaps $serve_repo";
	next unless $serve_repo =~ s{ ^/\~( [a-z][-+_0-9a-z]* )/ }{/}xi;
	$serve_user= $1;
	$serve_dir= $d;
        syslog 'debug', "DEBUG $ARGV:$. match".
            " $serve_user $serve_dir $serve_repo";
    } elsif (m{^ repo-regexp \s+ (\S.*) $ }x) {
	$repo_regexp= $1;
    } elsif (m{^ (no-)?require-git-daemon-export-ok $ }x) {
	$check_export= !defined $1;
    } else {
	fail "config syntax error at $ARGV:$.";
    }
}

# end
