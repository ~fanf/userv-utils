#!/usr/bin/make -f

# Copyright 1996-2013,2016 Ian Jackson <ijackson@chiark.greenend.org.uk>
# Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
# Copyright 1999,2003
#    Chancellor Masters and Scholars of the University of Cambridge
# Copyright 2010 Tony Finch <fanf@dotat.at>
#
# This is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with userv-utils; if not, see http://www.gnu.org/licenses/.

subdirs_build=	ipif ucgi git-daemon
subdirs_nobuild=dyndns groupmanage misc
package=	userv-utils
packages_indep=	userv-dyndns userv-groupmanage userv-utils userv-git-daemon
packages_arch=	userv-ipif userv-cgi
packages=	$(packages_indep) $(packages_arch)

SHELL=/bin/bash

cwd=	$(shell pwd)
d=	$(cwd)/debian

%:
	dh $@

override_dh_auto_configure:

override_dh_auto_build:
	$(checkdir)
	set -e; for s in $(subdirs_build); do \
		$(MAKE) -C $$s all; \
	done
	touch build

override_dh_auto_install:
	$(checkdir)
	-
	set -e; for p in $(packages); do rm -rf $d/$$p; done
	-
	set -e; for s in $(subdirs_build) $(subdirs_nobuild); do \
		t=$d/userv-$$s; \
		rm -rf $$t; \
		$(MAKE) -C $$s install install-docs install-examples \
			prefix=$$t/usr \
			etcdir=$$t/etc \
			vardir=$$t/var \
			gituser=root \
		; \
	done
	-
	mv debian/userv-misc debian/userv-utils
	-
	mv $d/userv-ipif/usr/share/doc/userv-ipif/{service.c.txt,ipif.txt}
	-
	set -e; cd debian/userv-ucgi/usr/share/doc/userv-ucgi;	\
		rename 's/^/ucgi-/' *
	-
	set -e; cd debian/userv-groupmanage/usr/share/doc;	\
		mv groupmanage userv-groupmanage;		\
		rm userv-groupmanage/INSTALL
	-
	set -e; for p in userv-ipif userv-ucgi userv-groupmanage; do	\
		(cd debian/$$p/usr/share/doc;			\
		 mv $$p userv-utils);				\
		cp -al debian/$$p/. debian/userv-utils/.;	\
	done
	-
	perl -pe 's{/usr/local/}{/usr/}' ucgi/www-cgi	\
		>debian/userv-utils/etc/userv/services.d/www-cgi
	-
	rename 's/\.example$$//' $d/userv-ipif/etc/userv/vpn/*.example \
		$d/userv-dyndns/etc/userv/dyndns-service-users.example \
		$d/userv-utils/etc/groupmanage.conf.example
	rename 's/\.distrib$$//' $d/*/etc/userv/services.d/*.distrib
	-
	mv $d/userv-utils/usr/bin/mailq $d/userv-utils/usr/bin/mailq.userv
	-
	set -e; cd $d/userv-utils/etc/userv;			\
		mkdir services-available;			\
		mv services.d/* services-available/

override_dh_install:

override_dh_auto_clean:
	$(checkdir)
	rm -f build
	set -e; for s in $(subdirs_build); do \
		$(MAKE) -C $$s -i distclean || \
		$(MAKE) -C $$s -f Makefile.in distclean; \
	done
	rm -rf *~ debian/tmp debian/*~ debian/files* debian/substvars*

define checkdir
	test -f ipif/service.c
endef

# Below here is fairly generic really

checkroot:
	$(checkdir)
	test root = "`whoami`"
