m4_dnl udptunnel-vpn-config.m4: macros for udptunnel-reconf et al

m4_dnl This file is part of ipif, part of userv-utils
m4_dnl
m4_dnl Copyright 1996-2013 Ian Jackson <ijackson@chiark.greenend.org.uk>
m4_dnl Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
m4_dnl Copyright 1999,2003
m4_dnl    Chancellor Masters and Scholars of the University of Cambridge
m4_dnl Copyright 2010 Tony Finch <fanf@dotat.at>
m4_dnl
m4_dnl This is free software; you can redistribute it and/or modify it
m4_dnl under the terms of the GNU General Public License as published by
m4_dnl the Free Software Foundation; either version 3 of the License, or
m4_dnl (at your option) any later version.
m4_dnl
m4_dnl This program is distributed in the hope that it will be useful, but
m4_dnl WITHOUT ANY WARRANTY; without even the implied warranty of
m4_dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
m4_dnl General Public License for more details.
m4_dnl
m4_dnl You should have received a copy of the GNU General Public License
m4_dnl along with userv-utils; if not, see http://www.gnu.org/licenses/.

m4_define(V_JUNK,1)m4_divert(V_JUNK)
m4_define(V_VARIABLE,2)
m4_define(V_ACTIVES,3)
m4_define(V_PASSIVES,4)
m4_changequote([,])

m4_define(V_WARGS, [
 m4_define([$1], [m4_ifelse($][#,$2,[$3],
  [# ]m4___file__:m4___line__[: wrong number of args to $1])])])

V_WARGS([SET], 2, [V_SET([V_$1], [$2])])
m4_define(V_YES, [m4_define([V_SET],[m4_define($][@)])])
m4_define(V_NO, [m4_define([V_SET], [])])

m4_define(V_ACTPA, [
 V_WARGS($1, 1, [m4_divert(V_$1S)$][1 m4_dnl
 m4_divert(V_JUNK)m4_ifelse($][1,V_site, 
  [V_YES],
  [V_NO]
)])])

m4_dnl   V_WARGS(, 2, [m4_divert(V_PASSIVES)$1 m4_dnl
m4_dnl   m4_divert(V_JUNK)m4_ifelse([$1],V_site, [$2])])

m4_divert(V_JUNK)

V_YES
m4_include(V_defaults)

V_WARGS(SITE, 1, [m4_ifelse([$1],V_site, [V_YES], [V_NO])])
V_YES
m4_include(V_sites)
m4_undefine([SITE])

V_ACTPA(ACTIVE)
V_ACTPA(PASSIVE)
V_YES
m4_include(V_tunnels)

V_YES
m4_include(V_global)

m4_divert(V_VARIABLE)
WHVARIABLE
m4_divert(V_JUNK)

m4_define(V_WANTED, V_[]WANTED)
m4_divert(0)
m4_undivert(V_WANTED)
m4_divert(-1)
m4_undivert(V_JUNK)
m4_undivert(V_ACTIVES)
m4_undivert(V_PASSIVES)
m4_undivert(V_VARIABLE)
