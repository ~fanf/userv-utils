# Copyright 1996-2013 Ian Jackson <ijackson@chiark.greenend.org.uk>
# Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
# Copyright 1999,2003
#    Chancellor Masters and Scholars of the University of Cambridge
# Copyright 2010 Tony Finch <fanf@dotat.at>
#
# This is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with userv-utils; if not, see http://www.gnu.org/licenses/.

all:
	@echo >&2 'See README.  This is not a unified package.'

SUBDIRS_DISTCLEAN=	www-cgi ipif git-daemon

distclean:
	find . \( -name '*~' -o -name '#*#' -o -name '*.o' -o -name core \
		-o -name dist_tmp -o -name userv-utils-\*.tar.gz \
		-o -name '.#*' \) \
		-print0 | xargs -0r rm -rf --
	for f in $(SUBDIRS_DISTCLEAN); do make -C $$f distclean; done
