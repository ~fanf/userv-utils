/*
 * This file is part of ipif, part of userv-utils
 *
 * Copyright 1996-2013 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
 * Copyright 1999,2003
 *    Chancellor Masters and Scholars of the University of Cambridge
 * Copyright 2010 Tony Finch <fanf@dotat.at>
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv-utils; if not, see http://www.gnu.org/licenses/.
 */

#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "blowfish.h"

static void checkrw(int r, int exp_r, const char *op, FILE *f) {
  if (ferror(f)) { perror(op); exit(3); }
  if (feof(f)) { fprintf(stderr,"unexpected eof on %s\n",op); exit(2); }
  assert(r==exp_r);
}

int main(void) {
  struct blowfish_cbc_state cbc;
  unsigned char keybuf[BLOWFISH_MAXKEYBYTES], ivbuf[BLOWFISH_BLOCKBYTES];
  unsigned char ibuf[BLOWFISH_BLOCKBYTES], obuf[BLOWFISH_BLOCKBYTES];
  int r;

  r= fread(keybuf,1,sizeof(keybuf),stdin); checkrw(r,sizeof(keybuf),"input",stdin);
  blowfish_loadkey(&cbc.ek,keybuf,sizeof(keybuf));

  r= fread(ibuf,1,sizeof(ivbuf),stdin); checkrw(r,sizeof(ivbuf),"input",stdin);
  blowfish_cbc_setiv(&cbc,ivbuf);

  for (;;) {
    r= fread(ibuf,1,sizeof(ibuf),stdin); if (r<sizeof(ibuf) && r>=0) break;
    checkrw(r,sizeof(ibuf),"input",stdin);
    blowfish_cbc_encrypt(&cbc,ibuf,obuf);
    r= fwrite(obuf,1,sizeof(obuf),stdout); checkrw(r,sizeof(obuf),"output",stdout);
  }
  memset(ibuf+r,sizeof(ibuf)-r,sizeof(ibuf)-r);
  blowfish_cbc_encrypt(&cbc,ibuf,obuf);
  r= fwrite(obuf,1,sizeof(obuf),stdout); checkrw(r,sizeof(obuf),"output",stdout);
  return 0;
};
